# P8SCII encoder

This is a basic tool that receives a stream of bytes and converts them into a
string that would be properly read as that stream of bytes on the
[PICO-8](https://www.lexaloffle.com/pico-8.php) fantasy console.

PICO-8 uses UTF-8 when saving and loading files, but inside it uses a
single-byte codepage that is structured like this:

* 0-15: control codes, partially compatible with ASCII
* 16-31: additional narrow characters
* 32-127: narrow characters (uppercase uses a "puny" font), compatible with ASCII
* 128-255: "wide" characters -- graphics and hiragana/katakana

This program escapes all characters that need escaping and substitutes non-ASCII
characters with their PICO-8 counterparts. It may be useful for encoding raw
data or samples as a PICO-8 string.

## Usage (on Windows)

You can find the latest Windows version of this program on the [**releases
page**](https://gitlab.com/dev_urandom/p8scii-encoder/-/releases/).

When executed, you can either launch it straight-away (in which case it will
open a dialog box asking for a file) or drag-and-drop any file onto the icon of
its executable.

The program will create a text file *in UTF-8 encoding* with the converted
contents of the input file in the same directory as the original, with ".txt"
added to the end of the file name.

## Usage (on Linux)

To run the program on Linux, it has to be compiled. It only uses the standard C
libraries, so if you have `gcc` and `make` installed, just typing

```
make
```

should do the trick and create an executable file named `p8scii-encoder`.

The program can read data from the standard input (keyboard) or a file. To read
from standard input, launch without any parameters.

To read from a file, add an argument consisting of the file name to the end, for
example:

```
./p8scii-encoder file.bin
```

The program will create a new file with the suffix ".txt" added to its name and
put its output there.

To write to a specific file, use the "-o" argument, followed by the name of the
output file:

```
./p8scii-encoder -o string.txt
```

To both read from and write to a specific file, specify first the output file
with the "-o" argument, then the input file:

```
./p8scii-encoder -o string.txt file.bin
```



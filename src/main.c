#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef _WIN32
#include <windows.h>
#endif

const char* lowchars[16] = {"▮", "■", "□", "⁙", "⁘", "‖", "◀", "▶", "「", "」", "¥", "•", "、", "。", "゛", "゜" };

const char* highchars[128] = {
	"█", "▒", "🐱", "⬇️", "░", "✽", "●", "♥", "☉", "웃", "⌂", "⬅️", "😐", "♪", "🅾️", "◆", "…", "➡️", "★", "⧗", "⬆️", "ˇ", "∧", "❎", "▤", "▥", "あ", "い", "う", "え", "お", "か", "き", "く", "け", "こ", "さ", "し", "す", "せ", "そ", "た", "ち", "つ", "て", "と", "な", "に", "ぬ", "ね", "の", "は", "ひ", "ふ", "へ", "ほ", "ま", "み", "む", "め", "も", "や", "ゆ", "よ", "ら", "り", "る", "れ", "ろ", "わ", "を", "ん", "っ", "ゃ", "ゅ", "ょ", "ア", "イ", "ウ", "エ", "オ", "カ", "キ", "ク", "ケ", "コ", "サ", "シ", "ス", "セ", "ソ", "タ", "チ", "ツ", "テ", "ト", "ナ", "ニ", "ヌ", "ネ", "ノ", "ハ", "ヒ", "フ", "ヘ", "ホ", "マ", "ミ", "ム", "メ", "モ", "ヤ", "ユ", "ヨ", "ラ", "リ", "ル", "レ", "ロ", "ワ", "ヲ", "ン", "ッ", "ャ", "ュ", "ョ", "◜", "◝"};

int main(int argc, char** argv) {

	int c;

	FILE* infile = stdin;
	FILE* outfile = stdout;

	const char* inname = NULL;
	const char* outname = NULL;

#ifdef _WIN32
	HWND console = GetConsoleWindow();

	if (!console) {
		if (argc > 1) {
		} else {

			// use GetOpenFileName to select the file
			OPENFILENAME ofn;
			TCHAR szFile[PATH_MAX] = {0};

			memset(&ofn,0,sizeof ofn);
			ofn.lStructSize = sizeof ofn;
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = PATH_MAX;
			ofn.lpstrTitle = "Which file should be converted into a PICO-8 string?";

			if (GetOpenFileName(&ofn) == TRUE) {
				inname = strdup(ofn.lpstrFile);
			} else {

				MessageBox(NULL,"Please select a file to be converted using the dialog box or drag its icon onto this program's executable.","P8SCII Encoder",MB_OK | MB_ICONERROR);
				return 1;

			}

		}
	}
#endif

	int opt;
	while ((opt = getopt(argc,argv,"ho:")) != -1) {
		switch(opt) {
			case 'o':
				outname = optarg;
				break;
			case 'h':
			case '?':
				fprintf(stderr,"Usage: %s [-o output_file] [input_file]\n",argv[0]);
				return 1;
				break;
		}
	}

	if (optind < argc) {
		inname = argv[optind];
	}

	if (inname) {
		infile = fopen(inname,"rb");
		if (!infile) { perror("Unable to open input file"); return 1;}

		if (!outname) {
			char* newout = malloc(strlen(inname) + 4 + 1);
			if (!newout) { puts("Unable to allocate memory for output filename!"); return 1; }
			strcpy(newout,inname);
			strcat(newout,".txt");
			outname = newout;
		}
	}

	if (outname) {
		outfile = fopen(outname,"wb");
		if (!outfile) { perror("Unable to open input file"); return 1;}
	}

	fputs("\"",outfile);
	while ((c = fgetc(infile)) != EOF) {
		if ((c >= 16) && (c < 32)) 
			fputs(lowchars[c-16],outfile);
		else if (c >= 128) 
			fputs(highchars[c-128],outfile);
		else switch(c) {
			case 0: fputs("\\000",outfile); break;
			case 1: fputs("¹",outfile); break;
			case 2: fputs("²",outfile); break;
			case 3: fputs("³",outfile); break;
			case 4: fputs("⁴",outfile); break;
			case 5: fputs("⁵",outfile); break;
			case 6: fputs("⁶",outfile); break;
			case 7: fputs("⁷",outfile); break;
			case 8: fputs("⁸",outfile); break;
			case 9: fputs("\\t",outfile); break;
			case 10: fputs("\\n",outfile); break;
			case 11: fputs("ᵇ",outfile); break;
			case 12: fputs("ᶜ",outfile); break;
			case 13: fputs("\\r",outfile); break;
			case 14: fputs("ᵉ",outfile); break;
			case 15: fputs("ᶠ",outfile); break;
			case 92: fputs("\\\\",outfile); break;
			case 34: fputs("\\\"",outfile); break;
			default: fputc(c, outfile); break;
		}
	}
	fputs("\"",outfile);

	if (infile != stdin) fclose(infile);
	if (outfile != stdout) fclose(outfile);

	return 0;
}

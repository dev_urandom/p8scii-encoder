-include Makefile.cfg

OBJDIR=obj
SRCDIR=src

_CC=gcc
LD=ld
CFLAGS = -std=c11 -Wall
DEFS += -D_POSIX_C_SOURCE=2
LDFLAGS =

_OBJS = main.o

ifdef WINDOWS
CCPREFIX=x86_64-w64-mingw32-
OBJSUFFIX=.exe
LDFLAGS=-mwindows
endif

ifdef PROFILE
CFLAGS += -pg
LDFLAGS += -pg
endif

ifdef RELEASE
DEFS += -xSSE3 -O3 -DNDEBUG
else
DEFS += -g -O0
endif

CC = $(CCPREFIX)$(_CC)
OBJS = $(patsubst %,$(OBJDIR)/%,$(_OBJS))

install: p8scii-encoder$(OBJSUFFIX)

p8scii-encoder$(OBJSUFFIX): $(OBJS)
	$(CC) -o $@ $^ $(DEFS) $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(OBJDIR)
	$(CC) -c -o $@ $< $(CFLAGS) $(DEFS)

$(OBJDIR):
	mkdir -p $(OBJDIR)

clean:
	rm -rf $(OBJDIR)/*.o p8scii-encoder

.PHONY: clean install

